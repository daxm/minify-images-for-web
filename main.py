from decouple import config
from os import listdir
from os.path import isfile, join, splitext

import tinify

api_key = config('API_KEY')
full_path_to_images = config('FULL_PATH_OF_IMAGE_DIR')
COMPRESSIBLE_IMAGE_EXTENSIONS = ['.png', '.jpg', '.jpeg', '.gif']

if __name__ == '__main__':
    tinify.key = api_key
    image_dir = full_path_to_images
    images = []
    # images = [join(image_dir, f) for f in listdir(image_dir) if isfile(join(image_dir, f))]
    for file in listdir(image_dir):
        extension = splitext(file)[1]
        file = join(image_dir, file)
        if isfile(file) and extension.lower() in COMPRESSIBLE_IMAGE_EXTENSIONS:
            images.append(join(image_dir, file))
    for image in images:
        print(f"Compressing {image}")
        source = tinify.from_file(image)
        source.to_file(image)
