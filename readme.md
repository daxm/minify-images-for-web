# Minify your images for web usage

1. Go to https://tinypng.com/developers and create an account.
2. Access your account and create API key.
3. Copy .env-template to .env
4. Put API key in .env file
5. Put full path to images directory in .env file.
6. Run main.py file.
